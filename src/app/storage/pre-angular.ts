export class PreAngular {
  public static _instance: PreAngular = new PreAngular();

  public static bootstrap() {
    if (!this.isLoggedIn()) {
      localStorage.setItem("isLoggedIn", JSON.stringify(false));
      localStorage.setItem("auth", JSON.stringify({}));
      localStorage.setItem("userInfo", JSON.stringify({}));
      localStorage.setItem("currentStatus", JSON.stringify({}));
    }
  }

  public static setSession(auth: any) {
    localStorage.setItem("isLoggedIn", JSON.stringify(true));
    localStorage.setItem("auth", JSON.stringify(auth));        

  }

  public static refreshSession(auth: any) {
    localStorage.setItem("isLoggedIn", JSON.stringify(true));
    localStorage.setItem("auth", JSON.stringify(auth));
  }

  public static clearAll() {
    localStorage.setItem("isLoggedIn", JSON.stringify(false));
    localStorage.setItem("auth", JSON.stringify({}));
    localStorage.setItem("userInfo", JSON.stringify({}));
    
  }

  public static isLoggedIn(): boolean {
    return JSON.parse(localStorage.getItem("isLoggedIn"));
  }

  public static getAuth(): any {
    return JSON.parse(localStorage.getItem("auth"));
  }

  public static getUserInfo(): any {
    return JSON.parse(localStorage.getItem("userInfo"));
  }

  public static getCurrentCompanyGroupIndex(): number {
    return JSON.parse(localStorage.getItem("currentCompanyGroupIndex"));
  }

  public static getCurrentCompanyGroup(): any {
    return this.getUserInfo().company[this.getCurrentCompanyGroupIndex()];
  }

  public static getUserRole() {
    return this.getUserInfo().company[PreAngular.getCurrentCompanyGroupIndex()]
      .role;
  }

  public static setSegmentDetail(segmentDetail: any) {
    localStorage.setItem("segmentDetail", JSON.stringify(segmentDetail));
  }
  public static getSegmentDetail() {
    return JSON.parse(localStorage.getItem("segmentDetail"));
  }


  public static setCategoryDetail(categoryDetail: any) {
    localStorage.setItem("categoryDetail", JSON.stringify(categoryDetail));
  }
  public static getCategoryDetail() {
    return JSON.parse(localStorage.getItem("categoryDetail"));
  }

  public static setFiscalDetails(fiscalDetails: any) {
    localStorage.setItem("fiscalDetails", JSON.stringify(fiscalDetails));
  }

  public static getFiscalDetails(): any {
    return JSON.parse(localStorage.getItem("fiscalDetails"));
  }

  public static setJournalBatch(journalBatch: any) {
    localStorage.setItem("journalBatch", JSON.stringify(journalBatch));
  }

  public static getJournalBatch(): any {
    return JSON.parse(localStorage.getItem("journalBatch"));
  }
  public static setJournal(journal: any) {
    localStorage.setItem("journal", JSON.stringify(journal));
  }
  public static getJournal(): any {
    return JSON.parse(localStorage.getItem("journal"));
  }
  public static setAssetInfo(assetInfo: any) {
    localStorage.setItem("assetInfo", JSON.stringify(assetInfo));
  }
  public static getAssetInfo() {
    return JSON.parse(localStorage.getItem("assetInfo"));
  }
  public static getAuthorities(): any {
    return JSON.parse(localStorage.getItem('authorities'));
  }
  public static getUserType(): any {
    return JSON.parse(localStorage.getItem('auth')).userType;
  }
  public static getBranchCode(): any {
    return JSON.parse(localStorage.getItem('auth')).branchId;
  }
  public static setStatus(currentStatus:any) {
    localStorage.setItem("currentStatus", JSON.stringify(currentStatus));
  }
  public static getStatus() {
    return JSON.parse(localStorage.getItem("currentStatus"));
  }
}
