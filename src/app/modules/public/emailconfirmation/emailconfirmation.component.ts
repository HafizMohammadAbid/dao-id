import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute,ParamMap} from '@angular/router';
@Component({
  selector: 'app-emailconfirmation',
  templateUrl: './emailconfirmation.component.html',
  styleUrls: ['./emailconfirmation.component.scss']
})
export class EmailconfirmationComponent implements OnInit {

  email: string; 
  constructor(private route: ActivatedRoute) { }
 
  ngOnInit(){
  this.email = this.route.snapshot.paramMap.get('email');
  
  }

}
