import { Component, OnInit } from '@angular/core';
import { ActivatedRoute,ParamMap, Router} from '@angular/router';
import * as jwt_decode from "jwt-decode";
import { AuthService } from 'src/app/services/auth.service';
import { PreAngular } from 'src/app/storage/pre-angular';
@Component({
  selector: 'app-emailverification',
  templateUrl: './emailverification.component.html',
  styleUrls: ['./emailverification.component.css']
})
export class EmailverificationComponent implements OnInit {

  constructor(private route: ActivatedRoute,private authservice:AuthService,private router: Router) { }
  confirmationtoken: string;
  ngOnInit() {
    this.confirmationtoken = this.route.snapshot.paramMap.get('token');
    var jsonobject = {
      token : this.confirmationtoken
    };


    this.authservice.emailvalidation(jsonobject)
      .subscribe(data=>{
        PreAngular.setSession(data);
        var tokendata = jwt_decode(data.accesstoken);
        if (tokendata.profilestatus == 'pending' || tokendata.profilestatus == 'incomplete')
          this.router.navigateByUrl('/investor/dashboard');
          else {
          this.navigateAfterLogin();
          }


      },error=>{
        this.router.navigateByUrl('/public/login');

      })


  }

  navigateAfterLogin() {


    this.router.navigateByUrl('/investor/dashboard');

  }

}
