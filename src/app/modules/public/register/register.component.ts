import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validator, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { AppService } from 'src/app/app.service';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']

})
export class RegisterComponent implements OnInit {

  registerForm: FormGroup;

  constructor(private authservice: AuthService, private router: Router, private toast: ToastrService, private appService: AppService,private fb:FormBuilder) { }
  hide = true;
  strength = 0;
  ngOnInit() {
    this.registerForm = this.fb.group({
      firstname: new FormControl('', Validators.required),
      lastname: new FormControl('', Validators.required),
      middlename: new FormControl(''),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      confirmpassword: new FormControl('', [Validators.required,]),
      mobilephonenumber: new FormControl('')
    },{
      validator: this.MustMatch('password', 'confirmpassword')
   });
  }
  get f() { return this.registerForm.controls; }
  MustMatch(controlName: string, matchingControlName: string) {
    return (formGroup: FormGroup) => {
        const control = formGroup.controls[controlName];
        const matchingControl = formGroup.controls[matchingControlName];

        if (matchingControl.errors && !matchingControl.errors.mustMatch) {
            return;
        }

        if (control.value !== matchingControl.value) {
            matchingControl.setErrors({ mustMatch: true });
        } else {
            matchingControl.setErrors(null);
        }
    }
  }

  register() {
    this.appService.next(true);
    this.authservice.registeruser(this.registerForm.value)
      .subscribe(data => {

        this.toast.success('Successful', data.message, { positionClass: 'toast-bottom-right' });
        this.router.navigate(['/public/emailverificationsuccess', this.registerForm.value.email]);

      }, error => {

        this.toast.error(error, 'Error', { positionClass: 'toast-bottom-right' });
        this.appService.next(false);
      }, () => {
        this.appService.next(false);
      }
      )
  }

  onStrengthChanged(strength: number) {
    this.strength = strength;
  }

}
