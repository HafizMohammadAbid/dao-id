import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { LoginComponent } from './login/login.component';
import { PublicRoutingModule } from './public-routing.module';
import { PublicsidebarComponent } from './publicsidebar/publicsidebar.component';
import { RegisterComponent } from './register/register.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { MatPasswordStrengthModule } from '@angular-material-extensions/password-strength';

import { EmailconfirmationComponent } from './emailconfirmation/emailconfirmation.component';
import { EmailverificationComponent } from './emailverification/emailverification.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { OtpPopupComponent } from '../shared/otp-popup/otp-popup.component';
import { EmailVerificationRequestComponent } from './email-verification-request/email-verification-request.component';
import { MarketPlaceListingComponent } from './market-place-listing/market-place-listing.component';
@NgModule({
  imports: [
    SharedModule,
    PublicRoutingModule,
    MatPasswordStrengthModule,
  ],
  declarations: [
    LoginComponent,
    PublicsidebarComponent,
    RegisterComponent,
    ForgotpasswordComponent,
    ResetpasswordComponent,
    EmailconfirmationComponent,
    EmailverificationComponent,
    EmailVerificationRequestComponent,
    MarketPlaceListingComponent,
  ],
  entryComponents: [OtpPopupComponent],
  providers: []
})
export class PublicModule { }
