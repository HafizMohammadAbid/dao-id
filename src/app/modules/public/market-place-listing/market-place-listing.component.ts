import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { AuthService } from 'src/app/services/auth.service';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-market-place-listing',
  templateUrl: './market-place-listing.component.html',
  styleUrls: ['./market-place-listing.component.scss']
})
export class MarketPlaceListingComponent implements OnInit {
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  displayedColumns: string[] = ['areaunits', 'sqftprice', 'created_at', 'contextmenu'];
  dataSource = new MatTableDataSource();
  constructor(protected authService:AuthService) { }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;  
    this.authService.getMarketPlaceData()
    .subscribe(data=>{
      this.dataSource.data =    data.data;
    },error=>{


    },
    ()=>{
    })
  }

}
