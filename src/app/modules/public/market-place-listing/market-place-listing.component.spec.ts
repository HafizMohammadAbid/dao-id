import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MarketPlaceListingComponent } from './market-place-listing.component';

describe('MarketPlaceListingComponent', () => {
  let component: MarketPlaceListingComponent;
  let fixture: ComponentFixture<MarketPlaceListingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MarketPlaceListingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MarketPlaceListingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
