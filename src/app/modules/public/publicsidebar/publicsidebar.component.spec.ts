import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PublicsidebarComponent } from './publicsidebar.component';

describe('PublicsidebarComponent', () => {
  let component: PublicsidebarComponent;
  let fixture: ComponentFixture<PublicsidebarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PublicsidebarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PublicsidebarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
