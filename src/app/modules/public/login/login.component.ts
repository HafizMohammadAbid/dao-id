import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AppService } from 'src/app/app.service';
import { ToastrService } from 'ngx-toastr';
import { Router} from '@angular/router';
import * as jwt_decode from "jwt-decode";
import { AuthService } from 'src/app/services/auth.service';
import { PreAngular } from 'src/app/storage/pre-angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers:[AuthService]
})
export class LoginComponent implements OnInit {
  showError = false;
  errorDescription = '';
  rememberme: boolean = false;
  loginForm: FormGroup;
  public companyName: string= "Login"
  hide = true;

  constructor(  private authService: AuthService, private appService:AppService,private toastr:ToastrService,private router: Router) {
    if (PreAngular.isLoggedIn()) {
        this.navigateAfterLogin();
    }
  }

  ngOnInit() {
      this.loginForm = new FormGroup({
      email: new FormControl('', [Validators.required,Validators.email]),
      password: new FormControl('', Validators.required)
    });

  }
  signIn() {
    console.log(this.loginForm.value);
    if (this.loginForm.valid) {
      this.errorDescription = '';
      this.appService.next(true);
      this.authService.loginuser(this.loginForm.value)
        .subscribe(data=>{

          PreAngular.setSession(data);
          // this.toastr.success('Successful','Login successfully into app',{positionClass: 'toast-bottom-right'});
          this.appService.successmessage('Successful','Login successfully')
          var tokendata = jwt_decode(data.accesstoken);
          console.log("tokendata",tokendata);
          // if (tokendata.profilestatus == 'incomplete')
          //   this.router.navigateByUrl('/investor/profilesetup');
          // else {
          // this.navigateAfterLogin();
          // }
          this.navigateAfterLogin();
        },error=>{
          console.log(error);
          this.appService.next(false);
          this.appService.errormessage('Error',error);

        },
        ()=>{
          this.appService.next(false);
        })
    }
  }


  navigateAfterLogin() {


    this.router.navigateByUrl('/investor/dashboard');

  }
}
