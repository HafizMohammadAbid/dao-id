import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppService } from 'src/app/app.service';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-forgotpassword',
  templateUrl: './forgotpassword.component.html',
  styleUrls: ['./forgotpassword.component.scss']
})
export class ForgotpasswordComponent implements OnInit {

  forgotpasswordform: FormGroup;
  faChevronLeft = faChevronLeft;
  constructor(private authservice: AuthService, private appservice: AppService, private toast: ToastrService) { }

  ngOnInit(): void {
    this.forgotpasswordform = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email])
    });
  }

  forgotpasswordfunction() {
    this.appservice.next(true);
    console.log(this.forgotpasswordform.value);
    this.authservice.requestresetpassword(this.forgotpasswordform.value)
      .subscribe(data => {
        this.toast.success('We have sent you a password reset link. Check back in a few minutes if you didn’t receive!',
          'Reset link sent!', { positionClass: 'toast-bottom-right' });
      }, error => {
        this.toast.error(error, 'Error !!', { positionClass: 'toast-bottom-right' });
        this.appservice.next(false);
      },
        () => {
          this.appservice.next(false);
        });
  }
}
