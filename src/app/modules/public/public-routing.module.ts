import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ForgotpasswordComponent} from './forgotpassword/forgotpassword.component';
import { EmailconfirmationComponent } from './emailconfirmation/emailconfirmation.component';
import { EmailverificationComponent } from './emailverification/emailverification.component';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import { EmailVerificationRequestComponent } from './email-verification-request/email-verification-request.component';

import { MarketPlaceListingComponent } from './market-place-listing/market-place-listing.component';
const publicRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'forgotpassword',
    component: ForgotpasswordComponent
  },

  {
    path: 'emailverificationsuccess/:email',
    component: EmailconfirmationComponent
  },

  {
    path: 'emailtoken/:token',
    component: EmailverificationComponent
  },

  {
    path: 'reset/:token',
    component: ResetpasswordComponent
  },

  {
    path: 'resendtoken',
    component: EmailVerificationRequestComponent
  },
  {
    path: 'marketplace',
    component: MarketPlaceListingComponent
  }

];

@NgModule({
  imports: [
    RouterModule.forChild(publicRoutes),
  ],
  declarations: [
  ],
  providers: [],
  exports: [
    RouterModule
  ]
})
export class PublicRoutingModule { }
