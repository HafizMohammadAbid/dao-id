import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EmailVerificationRequestComponent } from './email-verification-request.component';

describe('EmailVerificationRequestComponent', () => {
  let component: EmailVerificationRequestComponent;
  let fixture: ComponentFixture<EmailVerificationRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EmailVerificationRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EmailVerificationRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
