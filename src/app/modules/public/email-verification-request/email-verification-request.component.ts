import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppService } from 'src/app/app.service';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-email-verification-request',
  templateUrl: './email-verification-request.component.html',
  styleUrls: ['./email-verification-request.component.scss']
})
export class EmailVerificationRequestComponent implements OnInit {

  emailverificationform: FormGroup;
  faChevronLeft = faChevronLeft;
  constructor(private authservice: AuthService, private appService: AppService, private toast: ToastrService) { }

  ngOnInit(): void {
    this.emailverificationform = new FormGroup({
      email: new FormControl('', [Validators.required, Validators.email])
    });
  }

  emailverificationfunction() {
    this.appService.next(true);
    console.log(this.emailverificationform.value);
    this.authservice.emailverificationtoken(this.emailverificationform.value)
      .subscribe(data => {

        this.appService.successmessage('Successful',data.message);
      }, error => {

        this.appService.errormessage('Error',error);
        this.appService.next(false);
      },
        () => {
          this.appService.next(false);
        });
  }

}
