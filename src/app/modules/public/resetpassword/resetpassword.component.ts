import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { AppService } from 'src/app/app.service';
import { faChevronLeft } from '@fortawesome/free-solid-svg-icons';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';
@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {

  Resetpasswordform : FormGroup;
  faChevronLeft = faChevronLeft;

  constructor(private route: ActivatedRoute,private authservice:AuthService,private appservice:AppService,private router: Router) {
    // this.appservice.next(true);
  }
   hide=true;
  resettoken: string;
  ngOnInit(): void {
    this.resettoken = this.route.snapshot.paramMap.get('token');


    this.Resetpasswordform =  new FormGroup({
      password: new FormControl('',[Validators.required]),
      confirmpassword: new FormControl('',[Validators.required]),
      passwordreseturl: new FormControl(this.resettoken,[Validators.required])
    });


  }


  Resetpasswordfunction(){
    this.appservice.next(true);

    this.authservice.passreset(this.Resetpasswordform.value)
      .subscribe(data=>{
        this.appservice.successmessage("Successfull",'Your password has been reset succesfully.');
         this.router.navigateByUrl('/public/login');
         this.appservice.next(false);
      },error=>{
        this.appservice.errormessage("Error",'Error occurred in resetting password');
        this.router.navigateByUrl('/public/login');
        this.appservice.next(false);
      });
  }
}
