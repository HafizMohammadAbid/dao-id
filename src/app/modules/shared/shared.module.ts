
import { NgModule } from '@angular/core';
import { AngularMaterialStubModule } from './angular-material-stub.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import {
  HttpClientModule,
  HTTP_INTERCEPTORS,
  HttpClient
} from '@angular/common/http';
import { SpinnerComponent } from './spinner/spinner.component';
import { ToastrModule } from 'ngx-toastr';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { PostBlockchainPopupComponent } from './post-blockchain-popup/post-blockchain-popup.component';
import { NgxMaskModule, IConfig } from 'ngx-mask'
import { DeviceDetectorModule } from 'ngx-device-detector';
import { PasswordPolicyComponent } from './password-policy/password-policy.component';
import { OtpPopupComponent } from './otp-popup/otp-popup.component';
import { QrCodePopupComponent } from './qrcode-popup/qrcode-popup.component';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';
import { DateAgoPipe } from './date-ago-pipe/date-ago.pipe';
import { MinusPipe } from './minus-pipe/minus.pipe';
@NgModule({

  imports: [
    AngularMaterialStubModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    HttpClientModule,
    ToastrModule.forRoot(),
    FontAwesomeModule,
    NgxMaskModule.forRoot(),
    DeviceDetectorModule,
    NgxQRCodeModule 
  ],
  declarations: [
    SpinnerComponent, PostBlockchainPopupComponent, MinusPipe, PasswordPolicyComponent,OtpPopupComponent,QrCodePopupComponent,DateAgoPipe
  ],
  exports: [
    AngularMaterialStubModule,
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SpinnerComponent,
    FontAwesomeModule,
    PostBlockchainPopupComponent,
    NgxMaskModule,
    MinusPipe,
    DeviceDetectorModule,
    PasswordPolicyComponent,
    OtpPopupComponent,
    QrCodePopupComponent,
    NgxQRCodeModule,
    DateAgoPipe
  ],
  providers: [
  ]
})
export class SharedModule { }
