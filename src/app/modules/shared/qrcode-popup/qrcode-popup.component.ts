import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-qrcode-popup',
  templateUrl: './qrcode-popup.component.html',
  styleUrls: ['./qrcode-popup.component.scss']
})
export class QrCodePopupComponent implements OnInit {

  title = 'authenticator';
  elementType = 'url';
  value = 'Techiediaries';
  constructor(
    public dialogRef: MatDialogRef<QrCodePopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log("Data",data.otp[0]);
      this.value = data.otp[0];
     }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {

  }

  submit(){
    this.dialogRef.close({
      password:this.data.password
    })

  }
}
