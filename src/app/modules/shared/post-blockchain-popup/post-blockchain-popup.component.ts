import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-post-blockchain-popup',
  templateUrl: './post-blockchain-popup.component.html',
  styleUrls: ['./post-blockchain-popup.component.scss']
})
export class PostBlockchainPopupComponent implements OnInit {


  constructor(
    public dialogRef: MatDialogRef<PostBlockchainPopupComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      console.log("Dialog data",data);
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {

  }

  submit(){
    this.dialogRef.close({
      password:this.data.password
    })

  }
}
