import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { AppService } from 'src/app/app.service';
@Injectable({
  providedIn: 'root'
})
export class SharedService {


  constructor(public http: HttpClient,
    private appService: AppService,
    private router: Router,
  ) { 
   
  }



}
