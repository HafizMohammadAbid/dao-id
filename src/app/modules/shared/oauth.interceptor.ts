
import { HttpRequest, HttpHandler, HttpInterceptor, HTTP_INTERCEPTORS, HttpClient } from "@angular/common/http";
import { Injector, Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { Subject, Observable, throwError } from "rxjs";
import { catchError, switchMap, tap} from "rxjs/operators";
import { PreAngular } from 'src/app/storage/pre-angular';
import { environment } from 'src/environments/environment';

@Injectable()
export class OauthInterceptor implements HttpInterceptor {
    refreshTokenInProgress = false;

    tokenRefreshedSource = new Subject();
    tokenRefreshed$ = this.tokenRefreshedSource.asObservable();

    constructor(private injector: Injector, private router: Router,private http: HttpClient) {}

    addAuthHeader(request) {

       if (request.url.includes('assets') || request.url.includes('auth') ){
           return request;
       }

        const authHeader = PreAngular.getAuth().accesstoken;
        if (authHeader) {
            return request.clone({
                setHeaders: {
                    "Authorization": authHeader
                }
            });
        }
        return request;
    }


  callRefreshToken() {
    var refreshtoken=PreAngular.getAuth().refreshtoken;
    var jsonobject = {
      sessionrefreshtoken : refreshtoken
    };
    console.log(refreshtoken);
     return this.http.post(environment.serviceUrl + 'auth/refreshaccesstoken', jsonobject);

  }
    refreshToken(): Observable<any> {
        if (this.refreshTokenInProgress) {
            return new Observable(observer => {
                this.tokenRefreshed$.subscribe(() => {
                    observer.next();
                    observer.complete();
                });
            });
        } else {
            this.refreshTokenInProgress = true;

            return this.callRefreshToken().pipe(
                tap((data:any) => {
                    PreAngular.setSession(data);
                    this.refreshTokenInProgress = false;
                    this.tokenRefreshedSource.next();
                }),
                catchError((data:any) => {
                    this.refreshTokenInProgress = false;
                    this.logout();
                    return Observable.throw(null);
                }));
        }
    }

    logout() {
        PreAngular.clearAll();
              if (window.location.href.indexOf('/public/login') == -1) {
                  this.router.navigate(['/public/login']);
              }
    }

    handleResponseError(error, request?, next?) {
        // Business error
        if (error.status === 400) {
            // Show message
        }

        // Invalid token error
        else if (error.status === 401) {
            return this.refreshToken().pipe(
                switchMap(() => {
                    request = this.addAuthHeader(request);
                    return next.handle(request);
                }),
                catchError(e => {
                    if (e.status !== 401) {
                        return this.handleResponseError(e);
                    } else {
                        this.logout();
                    }
                }));
        }

        // Access denied error
        else if (error.status === 403) {
            // Show message
            // Logout
            this.logout();
        }

        // Server error
        else if (error.status === 500) {
            // Show message
        }

        // Maintenance error
        else if (error.status === 503) {
            // Show message
            // Redirect to the maintenance page
        }

        return throwError(error);
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {





        // Handle request
        request = this.addAuthHeader(request);

        // Handle response
        return next.handle(request).pipe(catchError(error => {
            return this.handleResponseError(error, request, next);
        }));

    }
}
