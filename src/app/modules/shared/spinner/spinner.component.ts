import { Component, OnInit, Input, ElementRef, Renderer2, OnChanges } from '@angular/core';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit, OnChanges {
  @Input() showSpinner = false;
  @Input() type: string;
  matDrawer: HTMLElement;
  constructor(private _elementRef: ElementRef, private renderer: Renderer2) { }

  ngOnInit() {
  }

  ngOnChanges() {
    this.matDrawer = document.getElementsByClassName('mat-drawer-content')[0] as HTMLElement;
    if (this.showSpinner) {
      this.matDrawer === undefined ? this.renderer.setStyle(document.body, 'overflow', 'hidden') : this.matDrawer.style.overflow = 'hidden';
    } else {
      this.matDrawer === undefined ? this.renderer.setStyle(document.body, 'overflow', 'auto') : this.matDrawer.style.overflow = 'auto';
    }

  }

}
