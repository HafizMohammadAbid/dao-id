import { ToastrService } from 'ngx-toastr';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class AppService {

  formInProgress: BehaviorSubject<any[]> = new BehaviorSubject<any[]>([]);

  constructor(public http: HttpClient,private toastr: ToastrService) { }

  clear() {
    this.formInProgress.next([]);
  }

  next(data: boolean) {
    data ? (<Array<any>>this.formInProgress.getValue()).push(data) : (<Array<any>>this.formInProgress.getValue()).pop();
    this.formInProgress.next(this.formInProgress.getValue());
  }

  successmessage(title,message){
    this.toastr.success(message,title,{positionClass: 'toast-bottom-right'});
  }


  errormessage(title,message){

    this.toastr.error(message,title,{positionClass: 'toast-bottom-right'});
  }



}
