
import { NgModule, Optional, SkipSelf } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";
import { SelectivePreloadingStrategy } from "./selective-preloading-strategy";

import { PageNotFoundComponent } from "./page-not-found/page-not-found.component";
import { AuthGuardService } from './guards/auth-guard.service';
import { AuthService } from './services/auth.service';

const appRoutes: Routes = [

  { path: '', redirectTo: 'public/login', pathMatch: 'full' },
  {
    path: 'public', loadChildren: () => import('./modules/public/public.module').then(m => m.PublicModule)
  },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes, {
      preloadingStrategy: SelectivePreloadingStrategy,
      useHash: false
    })
  ],

  declarations: [],
  exports: [RouterModule],
  entryComponents: [],
  providers: [AuthService, SelectivePreloadingStrategy]
})
export class AppRoutingModule {
  constructor(@Optional() @SkipSelf() parentModule: AppRoutingModule) {
    if (parentModule) {
      throw new Error(
        "AppRoutingModule is already loaded. Import it in the AppModule only"
      );
    }
  }
  static forRoot() {
    return {
      ngModule: AppRoutingModule,
      providers: [AuthService]
    };
  }
}
