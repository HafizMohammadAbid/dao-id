import { Injectable } from '@angular/core';
import {
  CanActivate, Router,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  CanActivateChild,
  NavigationExtras,
  CanLoad, Route, NavigationEnd, UrlTree, Resolve,
} from '@angular/router';

import { filter } from 'rxjs/operators';
import * as jwtdecode from 'jwt-decode';
import { Observable } from 'rxjs';
import { AuthService } from 'src/app/services/auth.service';
import { PreAngular } from 'src/app/storage/pre-angular';


@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate, CanActivateChild, CanLoad {

  constructor(private authService: AuthService, private router: Router) {
          router.events.pipe(
            filter(event => event instanceof NavigationEnd)
          ).subscribe((event: NavigationEnd) => {
            console.log(event.url);
            console.log(event);
          });
   }


  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {

    return true;
  }


  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean  {

    return;
  }

  cancheckifprofileiscompleted(route: Route): boolean {
     var tokendata = PreAngular.getAuth();
     var accesstokendata = jwtdecode(tokendata.accesstoken);

     if (accesstokendata.profilestatus ==  'complete')
      return true;

    else
     return false;



  }
  canLoad(route: Route ): boolean  {

      if (PreAngular.isLoggedIn() === true) {
        return true;
      } else {
          this.router.navigate(['public/login']);
        return false;
     }
  }

}

@Injectable({
  providedIn: 'root'
})
export class ProfilestatusGuardService implements  CanLoad,Resolve<any> {
  constructor(private authService: AuthService, private router: Router,) {
    router.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe((event: NavigationEnd) => {
      console.log(event.url);
      console.log(event);
    });
}




canLoad(route: Route ): boolean {



  var tokendata = PreAngular.getAuth();
     var accesstokendata = jwtdecode(tokendata.accesstoken);
     return true;
    //  if (accesstokendata.profilestatus ==  'completed'){
    //   return true;
    //  }

    // else {


    //  this.router.navigate(['investor/profilesetup']);
    // }
}
resolve(
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
): Observable<any>|Promise<any>|any {
  // var tokendata = PreAngular.getAuth();
  // var accesstokendata = jwtdecode(tokendata.accesstoken);
  // if (accesstokendata.profilestatus ==  'completed'){
  //   return true;
  //  }

  // else {
  //   this.router.navigate(['investor/404']);
  // }
  return this.authService.getprofilestatus();
}


}
