import { TestBed } from '@angular/core/testing';

import { AuthGuardService } from './auth-guard.service';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { RouterTestingModule } from '@angular/router/testing';
import { AuthService } from 'src/app/services/auth.service';


describe('AuthGuardService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports : [RouterTestingModule],
    providers : [AuthGuardService,
      AuthService,HttpClient,
      HttpHandler]
  }));

  it('should be created', () => {
    const service: AuthGuardService = TestBed.get(AuthGuardService);
    expect(service).toBeTruthy();
  });
});
