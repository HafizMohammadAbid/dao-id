import { AppService } from 'src/app/app.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import  {HTTP_INTERCEPTORS} from '@angular/common/http';
import { SharedModule } from './modules/shared/shared.module';
import { PublicModule } from './modules/public/public.module';
import { OauthInterceptor } from './modules/shared/oauth.interceptor';



@NgModule({
  declarations: [
    AppComponent,


  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,

    BrowserModule,
    AppRoutingModule.forRoot(),
    SharedModule,
    PublicModule,


  ],
  providers: [{provide:HTTP_INTERCEPTORS,useClass: OauthInterceptor,multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
