import { Injectable } from "@angular/core";
import {
  HttpResponse,
  HttpClient,
  HttpHeaders,
  HttpParams,
  HttpParameterCodec
} from "@angular/common/http";
import { Router } from "@angular/router";
import { Observable, BehaviorSubject,throwError } from "rxjs";
import { element } from "protractor";
import { environment } from "../../environments/environment";
import { catchError,map } from 'rxjs/operators';


@Injectable()
export class AuthService {
  constructor(private http: HttpClient, private router: Router) {
   }



  registeruser(request): Observable<any>{



    const apiURL = environment.serviceUrl+'auth/signup';
    return this.http.post(apiURL,request)
      .pipe(catchError(this.handlerror));

  }


  loginuser(request): Observable<any>{
    const apiURL=environment.serviceUrl+'auth/login';
    return this.http.post(apiURL,request)
      .pipe(catchError(this.handlerror));
  }

  requestresetpassword(request): Observable<any>{
      const apiURL=environment.serviceUrl+'auth/requestpasswordreset';
      return this.http.post(apiURL,request)
        .pipe(catchError(this.handlerror));
  }

  emailverificationtoken(request): Observable<any>{
    const apiURL=environment.serviceUrl+'auth/emailverification';
    return this.http.post(apiURL,request)
      .pipe(catchError(this.handlerror));
  }


  emailvalidation(request): Observable<any>{
    const apiURL = environment.serviceUrl+'auth/emailchecktoken';
    return this.http.post(apiURL,request)
      .pipe(catchError(this.handlerror));
  }

  getMarketPlaceData(): Observable<any>{
    const apiURL = environment.serviceUrl+'sell';
    return this.http.get(apiURL)
      .pipe(catchError(this.handlerror));
  }



  handlerror(error){

    var errormessage = '';
    if (error.error){
      errormessage = error.error.message;
    }else {
      errormessage = 'Error occurred';
    }

    return throwError(errormessage);
  }


  getprofilestatus():Observable<any>{
    const profilestatus = environment.serviceUrl+'profilestatus';
    return this.http.get(profilestatus)
      .pipe(catchError(this.handlerror));
  }


  passreset(formdata):Observable<any>{
    const apiUrl = environment.serviceUrl + 'auth/passreset';
    return this.http.post(apiUrl,formdata)
      .pipe(catchError(this.handlerror));
  }




}
