import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  formInProgress = false;
  constructor(    
    private appService: AppService,
  ) {}

  title = 'portalfrontend';
  ngOnInit(){
    this.appService.formInProgress.subscribe((res) => {
      console.log(res);
      if (this.appService.formInProgress.getValue().length > 0) {
        if (this.formInProgress === false) {
          this.formInProgress = true;          
        }
      } else {
        if(this.formInProgress === true) {
          this.formInProgress = false;          
        }
      }
    });
  }
  
}
